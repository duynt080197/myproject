#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Duy'
SITENAME = 'Blog của Duy'
SITEURL = ''

PATH = 'content'

THEME = 'notmyidea'

TIMEZONE = 'Asia/Ho_Chi_Minh'

DEFAULT_LANG = 'vi'

NEWEST_FIRST_ARCHIVES = False

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Học Python tại Hà Nội, Sài Gòn, TP HCM', 'https://pymi.vn'),
         ('Python.org', 'https://www.python.org/'),
         ('Web tuyển dụng python tui tạo sau 1,5 tháng học lập trình nè', 'https://jobspy.herokuapp.com/'),
         ('My linkedin', 'https://www.linkedin.com/in/duy-nguyen-623b881bb/'),
         )

# Social widget
SOCIAL = (('GitHub PyMiVN', 'https://github.com/pymivn/'),
          )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = False
